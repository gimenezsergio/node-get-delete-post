const { request, response } = require('express')
const express = require('express')
const app = express()

let listado = [
    {"id":1, "nombre": "Celular"},
    {"id":2, "nombre": "Notebook"},
    {"id":3, "nombre": "Mouse"},
    {"id":4, "nombre": "Teclado"},
    {"id":5, "nombre": "Cable hdmi"},
]

app.get('/', (request, response) => {
    response.send('<h1>Bienvenidos al home!</h1>')
})

app.get('/productos', (request, response) => {
    // response.send('<h1>Productos de oferta</h1>')
    response.json(listado)
})



app.get('/productos/:id', (request, response) => {
    const id = Number(request.params.id)
    const producto = listado.find( elemento => elemento.id === id)
    response.json(producto)
})


app.delete('/productos/:id', (request, response) => {
    const id = Number(request.params.id)
    listado = listado.filter( elemento => elemento.id !== id)
    response.send(`Vamos a borrar el producto ${id} y actualizar el listado`)

})

app.post('/productos', (request, response) => {
    const producto = request.body
    response.json(producto)
})



const port = 3000
app.listen(port, ()=>{console.log(`Server is running in port ${port}`)})